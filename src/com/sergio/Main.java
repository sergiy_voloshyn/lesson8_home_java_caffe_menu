package com.sergio;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        // write your code here
        /*

        2. Создать программу "Кафе". В ней организовать хранение блюд и их стоимость.
        Позволить пользователю выбирать блюда из списка, их количество и выдавать чек
        (вывести в консоль список блюд с их стоимостью и общей стоимостью по чеку).
         Для хранения блюд и данных заказа использовать HashMap.

         */


        HashMap<String, Float> menu = new HashMap();

        menu.put("борщ", 17.0F);
        menu.put("суп", 15.5F);
        menu.put("пюре", 8.0F);
        menu.put("спагетти", 13.8F);
        menu.put("котлета", 33.8F);
        menu.put("отбивная", 33.8F);
        menu.put("салат", 15.40F);
        menu.put("чай", 5.45F);
        menu.put("компот", 5.45F);

        System.out.println(menu.entrySet());


        HashMap<String, Integer> order = new HashMap();


        Scanner scan = new Scanner(System.in);

        //количество блюд
        System.out.println("Введите количество блюд: ");
        int numItems = Integer.parseInt(scan.nextLine());


        //Введите блюдо номер

        for (int i = 0; i < numItems; i++) {
            System.out.println("Введите название блюда: ");
            String position = scan.nextLine().toLowerCase();

            if (menu.containsKey(position.toLowerCase())) {
                System.out.println("Введите количество : ");
                int numbers = Integer.parseInt(scan.nextLine());
                order.put(position, numbers);
            }
        }
        System.out.println(order.entrySet());

        //to set
        Set<Map.Entry<String, Integer>> setOrder = order.entrySet();

        float total = 0;

        for (Map.Entry<String, Integer> itemOrder : setOrder) {
            System.out.println(itemOrder.getKey() + ":" + itemOrder.getValue() + " =");
            System.out.println(menu.get(itemOrder.getKey()) * itemOrder.getValue());
            total += menu.get(itemOrder.getKey()) * itemOrder.getValue();
        }
        System.out.println("Total:" + total);


    }


}
